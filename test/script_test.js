"use strict";

const test = require('tap').test;
const Path = require('path');
const fs = require('fs');

const dup = require('../index');

const simplePath = Path.join(__dirname, 'content/simple/');
const complexPath = Path.join(__dirname, 'content/faq-module/');

test("Duplicate simple content", function(t) {
    const copyPath = Path.join(__dirname, 'content/dups/copy_of_simple/');
    dup(simplePath, copyPath, function(err, dest) {
        fs.readFile(copyPath + 'index.html', 'utf8', function(error, content) {
            t.error(err);
            t.error(error);
            t.ok(/id="copy_of_simple"/.test(content));
            t.end();
        });
    });
});

test("Duplicate faq module content", function(t) {
    const copyPath = Path.join(__dirname, 'content/dups/copy-of-faq/');
    dup(complexPath, copyPath, function(err, dest) {
        fs.readFile(copyPath + 'copy-of-faq.html', 'utf8', function(error, content) {
            t.error(err);
            t.error(error);
            t.ok(/id="copy-of-faq"/.test(content));
            t.end();
        });
    });
});
