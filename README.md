# Anthill Duplicate Folder
[![build status](https://gitlab.com/anthill-modules/ah-duplicate-folder/badges/master/build.svg)](https://gitlab.com/anthill-modules/ah-duplicate-folder/commits/master)
[![coverage report](https://gitlab.com/anthill-modules/ah-duplicate-folder/badges/master/coverage.svg)](https://gitlab.com/anthill-modules/ah-duplicate-folder/commits/master)

Duplicate a folder while renaming files and replacing id strings.

## Install
```
npm install ah-duplicate-folder --save
```

## Usage
```
var duplicator = require('ah-duplicate-folder');

duplicator('content/faq-module', 'content/menu-module', function(err, dest) {
    if (!err) {
        console.log('Content was successfully duplicated to ' + dest);
    }
    else {
        console.log('Not able to duplicate folder', err);
    }
});
```
