'use strict';

const Path = require('path');
const gulp = require('gulp');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const gulpif = require('gulp-if');

module.exports = function(src, dest, next) {

    var srcFiles = Path.join(src, '**/*');
    var npmFile = Path.join(src, '.npmrc');
    var gitFile = Path.join(src, '.gitignore');
    var ignoreModules = Path.join("!" + src, 'node_modules', '**/*');
    var ignoreBuild = Path.join("!" + src, 'build', '**/*');
    var ignoreDist = Path.join("!" + src, 'dist', '**/*');
    var orgId = Path.basename(src);
    var id = Path.basename(dest);

    gulp.task('duplicate', function(done) {
        return gulp.src([srcFiles, npmFile, gitFile, ignoreModules, ignoreBuild, ignoreDist])
            .pipe(rename(function (path) {
                path.basename = path.basename.replace(orgId, id);
            }))
            .pipe(replace(orgId, id))
            .pipe(gulp.dest(dest))
            .on('end', function() {
                next(null, dest);
            })
            .on('error', function(err) {
                next(err, dest);
            });
    });

    gulp.start.apply(gulp, ['duplicate']);


};
